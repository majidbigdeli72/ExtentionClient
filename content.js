var hostname = window.location.hostname;
if (hostname.includes("jobinja.ir")) {
  handleJobInja();
} else if (hostname.includes("jobvision.ir")) {
  handleJobVision();
}

function appendHtml() {
  if (chrome && chrome.storage && chrome.storage.sync) {
    chrome.storage.sync.get(
      {
        username: "",
        password: "",
        url: "",
      },
      function (item) {
        if (item.username) {
          if (document.getElementById("#1st")) {
            document.getElementById("#1st").remove();
          }
          document.querySelectorAll(
            ".modal-footer.d-flex.bg-grey-light-3.justify-content-start button[type='submit']"
          )[3].innerText = "ارسال به crm";

          document
            .querySelector(".modal-content.border-0 .modal-body")
            .insertAdjacentHTML(
              "beforeend",
              `
                                  <div id="1st">
                                      <div class="font-size-md pt-2 pb-2">
                                          <label style="font-weight:700">توضیحات :</label>
                                          <textarea id="dec1st" placeholder="توضیحات ارسالی به crm" style="width: 100% !important;padding: 5px;"></textarea>
                                      </div>
                                      <div class="font-size-md pt-2 pb-2">
                                      <input id="impo1st" type="checkbox"></input>
                                      <label style="font-weight:700">رزومه با اهمیت زیاد بررسی شود</label>
                                      </div>
                                  </div>                    
                                    `
            );
        }
      }
    );
  }
}

function sendDataToCrm() {
  var important = document.getElementById("impo1st").checked;
  var approverNote = document.getElementById("dec1st").value;
  chrome.storage.sync.get(
    {
      username: "",
      password: "",
      url: "",
    },
    function (item) {
      var jobinjaAddress = document.querySelectorAll(
        "#userCV .fully-ltr.d-inline-block"
      )[3].innerText;
      var pathname = new URL(jobinjaAddress).pathname;
      //
      var email = document.querySelectorAll(
        "#userCV .fully-ltr.d-inline-block"
      )[2].innerText;
      var phone = fixNumbers(
        document.querySelectorAll("#userCV .fully-ltr.d-inline-block")[0]
          .innerText
      );
      var fullName = document
        .querySelectorAll(
          ".d-inline-flex.align-items-baseline.align-items-sm-center.flex-wrap.flex-column.flex-sm-row"
        )[0]
        .childNodes[0].textContent.trim();
      var approver = document
        .querySelectorAll(
          ".dropdown-header.pv-2.font-weight-bold.color-grey-dark-3"
        )[0]
        .childNodes[0].textContent.trim();

      var source = "jobinja";
      var title = document.querySelector(
        ".font-weight-bold.font-size-2xl.color-grey-dark-3.d-inline-block.sm-font-size-xl span"
      ).innerText;

      var fileAddress = document.querySelectorAll(
        ".card-header.font-weight-semiBold.font-size-semi-lg.border-bottom-1.border-solid.border-grey-light-1.card-header-transparent.bg-grey-light-2-prime a"
      )[0].href;

      var refId = pathname.split("/")[2];
      var endPointAddress = item.url;
      var userName = item.username;
      var password = item.password;
      var isHttps = new URL(endPointAddress).protocol.trim() == "https:".trim();

      var obj = {
        email: email,
        phone: phone,
        fullName: fullName,
        approver: approver,
        important: important,
        source: source,
        title: title,
        approverNote: approverNote,
        fileAddress: fileAddress,
        refId: refId,
        endPointAddress: endPointAddress,
        userName: userName,
        password: password,
        isHttps: isHttps,
      };
      var data = JSON.stringify(obj);
      var xhr = new XMLHttpRequest();
      xhr.withCredentials = true;
      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
          if (xhr.status === 200) {
            alert(xhr.responseText);
          } else {
            alert(xhr.statusText);
          }
        }
      });

      xhr.open(
        "POST",
        "https://service.payamgostar.com/api/Extention/SaveTicket"
      );
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.send(data);
    }
  );
}

var fixNumbers = function (str) {
  var persianNumbers = [
    /۰/g,
    /۱/g,
    /۲/g,
    /۳/g,
    /۴/g,
    /۵/g,
    /۶/g,
    /۷/g,
    /۸/g,
    /۹/g,
  ];
  var arabicNumbers = [
    /٠/g,
    /١/g,
    /٢/g,
    /٣/g,
    /٤/g,
    /٥/g,
    /٦/g,
    /٧/g,
    /٨/g,
    /٩/g,
  ];
  if (typeof str === "string") {
    for (var i = 0; i < 10; i++) {
      str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
    }
  }
  return str;
};

function handleJobInja() {
  document.addEventListener("click", function (e) {
    if (e.target.getAttribute("name") == "status-confirm") {
      appendHtml();
    } else if (e.target.innerText == "ارسال به crm") {
      sendDataToCrm();
    } else {
      return false;
    }
  });
}

function handleJobVision() {
  if (document.getElementById("CartableCVViewMode")) {
    document.getElementById(
      "CartableCVViewMode"
    ).innerHTML += `<div id="myModal1st" class="modal1st">

    <!-- Modal content -->
    <div class="modal-content1st">
      <div class="modal-header1st">
        <span class="close1st">&times;</span>
        <h5>ارسال رزومه به پیامگستر</h5>
      </div>
      <div class="modal-body1st">
      <div id="1st">
      <div class="font-size-md pt-2 pb-2">
          <label style="font-weight:700">توضیحات :</label>
          <textarea id="dec1st" placeholder="توضیحات ارسالی به crm" style="width: 100% !important;padding: 5px;"></textarea>
      </div>
      <div class="font-size-md pt-2 pb-2">
      <input id="impo1st" type="checkbox"></input>
      <label style="font-weight:700">رزومه با اهمیت زیاد بررسی شود</label>
      </div>
      </div> 
      </div>
      <div class="modal-footer1st">
        <button class="btn btn-success" id="sendtoCrm">ارسال به crm</button>
      </div>
    </div>
  
  </div>`;

    var applicationid = 0;

    var modal = document.getElementById("myModal1st");

    var span = document.getElementsByClassName("close1st")[0];

    span.onclick = function () {
      modal.style.display = "none";
    };

    if (document.querySelector("#staticPartOfCartableMode")) {
      document
        .querySelector("#staticPartOfCartableMode")
        .addEventListener("DOMNodeInserted", function (e) {
          if (event.target instanceof HTMLElement) {
            if (e.target.querySelector("#appStatus-con")) {
              document
                .querySelectorAll("#appStatus-con .first-row button")[3]
                .setAttribute("nid", "pri1");
              document
                .querySelectorAll("#appStatus-con .first-row button img")[3]
                .setAttribute("nid", "pri1");

              document
                .querySelectorAll("#appStatus-con .first-row button")[2]
                .setAttribute("nid", "pri2");

              document
                .querySelectorAll("#appStatus-con .first-row button img")[2]
                .setAttribute("nid", "pri2");
            }
          }
        });
    }
    document.addEventListener("click", function (e) {
      if (e.target.getAttribute("nid") == "pri1") {
        applicationid = e.target
          .closest(".descriminatorRow")
          .getAttribute("data-applicationid");
        document.getElementById("myModal1st").style.display = "block";
        document.getElementById("impo1st").checked = true;
        document.getElementById("dec1st").value = "";
      } else if (e.target.getAttribute("nid") == "pri2") {
        applicationid = e.target
        .closest(".descriminatorRow")
        .getAttribute("data-applicationid");
        document.getElementById("myModal1st").style.display = "block";
        document.getElementById("impo1st").checked = false;
        document.getElementById("dec1st").value = "";
      } else if (e.target.getAttribute("id") == "sendtoCrm") {
        var obj = getActiveResumeJobVission();
        sendToCrmJobvision(obj);
      }
    });

    function sendToCrmJobvision(obj) {
      var important = document.getElementById("impo1st").checked;
      var approverNote = document.getElementById("dec1st").value;
      chrome.storage.sync.get(
        {
          username: "",
          password: "",
          url: "",
        },
        function (item) {
          obj.important = important;
          obj.approverNote = approverNote;
          obj.endPointAddress = item.url;
          obj.userName = item.username;
          obj.password = item.password;
          obj.isHttps = new URL(item.url).protocol.trim() == "https:".trim();

          var data = JSON.stringify(obj);
          var xhr = new XMLHttpRequest();
          xhr.withCredentials = true;
          xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
              if (xhr.status === 200) {
                alert(xhr.responseText);
              } else {
                alert(xhr.statusText);
              }
            }
          });

          xhr.open(
            "POST",
            "https://service.payamgostar.com/api/Extention/SaveTicket"
          );
          xhr.setRequestHeader("Content-Type", "application/json");

          xhr.send(data);
          document.getElementById("myModal1st").style.display = "none";
        }
      );
    }

    function getActiveResumeJobVission() {
      var elem = document.querySelector(
        `[data-applicationid='${Number(applicationid)}'].CVItem`
      );

      var mobile = "";
      var email = "";

      Array.from(elem
        .querySelectorAll(".detail-item")).filter(el => {
       
          if (el.textContent.includes("Email:") || el.textContent.includes("ایمیل:")) {
            email = el.querySelector(".answer").textContent.trim();
          } else if (el.textContent.includes("Mobile:") || el.textContent.includes("موبایل:")) {
            mobile = fixNumbers(el.querySelector(".answer").textContent.trim());
          }
        })

      var fullName = elem.querySelector(".fullName").textContent.trim();
      var approver = document
        .querySelector(".companyUser a")
        .textContent.trim();
      var source = "jobvision";
      var title = document.querySelector("#jobTitle").textContent.trim();
      var fileAddress = elem.querySelector(
        ".CV-con:not(.hidden) .CvDownload-con a"
      ).href;
      var url = new URL(fileAddress);
      var refId = url.searchParams.get("userId");

      var obj = {
        email: email,
        phone: mobile,
        fullName: fullName,
        approver: approver,
        important: false,
        source: source,
        title: title,
        approverNote: "",
        fileAddress: fileAddress,
        refId: refId,
        endPointAddress: "",
        userName: "",
        password: "",
        isHttps: false,
      };

      return obj;
    }
  }
}

// important Send To BackGround And Get From
// chrome.runtime.sendMessage({ type: "bglog" }, function (response) {
//   console.log(response);
// });
