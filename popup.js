document.addEventListener(
  "DOMContentLoaded",
  function () {
    chrome.storage.sync.get(
      {
        username: "",
        password: "",
        url: "",
      },
      function (item) {
        if (item.username) {
          document.getElementById(
            "container"
          ).innerHTML = document.getElementById("complateTicket").innerHTML;
        } else {
          document.getElementById(
            "container"
          ).innerHTML = document.getElementById("formSave").innerHTML;
        }
        Save();
      }
    );
  },
  false
);

function Save() {
  var container = document.getElementById("container");
  SaveTicket(container);
  RestartTicket(container);
}

function SaveTicket(container) {
  container.addEventListener(
    "click",
    function (e) {
      if (e.target.getAttribute("id") != "submit") return false;
      var usernameObj = document.getElementById("username");
      var passwordObj = document.getElementById("password");
      var urlObj = document.getElementById("url");
      if (!usernameObj.checkValidity()) {
        return;
      } 
      if (!passwordObj.checkValidity()) {
        return;
      } 
      if (!urlObj.checkValidity()) {
        return;
      } 
      var username = usernameObj.value;
      var password = passwordObj.value;
      var url = new URL(urlObj.value).origin;

      chrome.storage.sync.set(
        {
          username: username,
          password: password,
          url: url,
        },
        function () {
          document.getElementById(
            "container"
          ).innerHTML = document.getElementById("complateTicket").innerHTML;
        }
      );
    },
    false
  );
}

function RestartTicket(container) {
  container.addEventListener(
    "click",
    function (e) {
      console.log(e.target);
      if (e.target.getAttribute("id") != "Restart") return false;

      chrome.storage.sync.set(
        {
          username: "",
          password: "",
          url: "",
        },
        function () {
          document.getElementById("container").innerHTML = document.getElementById("formSave").innerHTML;;
        }
      );
    },
    false
  );
}
